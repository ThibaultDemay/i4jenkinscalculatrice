/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package i4jenkinscalculatrice;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author thiba
 */
public class CalculatriceTest {
    
    public CalculatriceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of add method, of class Calculatrice.
     */
    @Test
    public void testAdd() {
        System.out.println("add");
        float a = 2.0F;
        float b = 5.0F;
        Calculatrice instance = new Calculatrice();
        float expResult = 7.0F;
        float result = instance.add(a, b);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of substract method, of class Calculatrice.
     */
    @Test
    public void testSubstract() {
        System.out.println("substract");
        float a = 2.0F;
        float b = 5.0F;
        Calculatrice instance = new Calculatrice();
        float expResult = -3.0F;
        float result = instance.substract(a, b);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of multiply method, of class Calculatrice.
     */
    @Test
    public void testMultiply() {
        System.out.println("multiply");
        float a = 2.0F;
        float b = 5.0F;
        Calculatrice instance = new Calculatrice();
        float expResult = 10.0F;
        float result = instance.multiply(a, b);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of divide method, of class Calculatrice.
     */
    @Test
    public void testDivide() {
        System.out.println("divide");
        float a = 2.0F;
        float b = 5.0F;
        Calculatrice instance = new Calculatrice();
        float expResult = 0.4F;
        float result = instance.divide(a, b);
        assertEquals(expResult, result, 0.0);
    }
    
    /**
     * Test of divide method, of class Calculatrice.
     * Specific test denominator equals 0
     */
    @Test (expected = IllegalArgumentException.class)
    public void testDivideByZero() {
        System.out.println("divide by 0");
        float a = 2.0F;
        float b = 0.0F;
        Calculatrice instance = new Calculatrice();
        float result = instance.divide(a, b);
    }
    
    
}
