/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package i4jenkinscalculatrice;

/**
 *
 * @author thiba
 */
public class Calculatrice {

    public Calculatrice() {

    }

    /**
     * Une super fonction d'ajout
     *
     * @param a un float
     * @param b un float
     */
    public float add(float a, float b) {
        return a + b;
    }

    /**
     * Une super fonction de soustraction
     *
     * @param a un float
     * @param b un float
     */
    public float substract(float a, float b) {
        return a - b;
    }

    /**
     * Une super fonction de multiplication
     *
     * @param a un float
     * @param b un float
     */
    public float multiply(float a, float b) {
        return a * b;
    }

    /**
     * Une super fonction de division
     *
     * @param a un float
     * @param b un float diff�rent de 0
     */
    public float divide(float a, float b) throws IllegalArgumentException {
        if (b != 0) {
            return a / b;
        } else {
            throw new IllegalArgumentException("Denominator cannot be equal to 0 !");
        }
    }

}
